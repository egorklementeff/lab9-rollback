import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedList;

public class Main {
    // JDBC driver name and database URL
    static final String DB_URL = "jdbc:postgresql://localhost:5432/company";

    //  Database credentials
    static final String USER = "postgres";
    static final String PASS = "pass";

    public static void main(String[] args) {
        Connection conn = null;
        Statement stmt = null;
        try {
            Class.forName("org.postgresql.Driver");
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL,USER,PASS);
            conn.setAutoCommit(false);


            System.out.println("Creating statement...");
            stmt = conn.createStatement(
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_UPDATABLE);

            // Inserts
            insertEmployee(108, 20, "Jane", "Eyre", conn, stmt);
            insertEmployee(109, 20, "David", "Rochester", conn, stmt);
            insertEmployee(106, 20, "Rita", "Tez", conn, stmt);
            insertEmployee(107, 22, "Sita", "Singh", conn, stmt);

            add_salaries(
                    new ArrayList<Integer>() {{add(1); add(2); add(3);}},
                    new ArrayList<Integer>() {{add(108); add(109); add(107);}},
                    new ArrayList<Integer>() {{add(10); add(20); add(30);}},
                    conn,
                    stmt
            );

            add_salaries(
                    new ArrayList<Integer>() {{add(4); add(5);}},
                    new ArrayList<Integer>() {{add(106); add(109);}},
                    new ArrayList<Integer>() {{add(50); add(60);}},
                    conn,
                    stmt
            );

            String sql = "SELECT id, first, last, age FROM Employees";
            ResultSet rs = stmt.executeQuery(sql);
            System.out.println("List result set for reference....");
            printRs(rs);
            rs.close();

            System.out.println();

            sql = "SELECT id, employee_id, amount FROM Salary";
            ResultSet rs1 = stmt.executeQuery(sql);
            printSal(rs1);
            rs1.close();

            stmt.close();
            conn.close();
        }catch(SQLException se){
            //Handle errors for JDBC
            se.printStackTrace();
            // If there is an error then rollback the changes.
            System.out.println("Rolling back data here....");
            try{
                if(conn!=null)
                    conn.rollback();
            }catch(SQLException se2){
                se2.printStackTrace();
            }//end try

        }catch(Exception e){
            //Handle errors for Class.forName
            e.printStackTrace();
        }finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                    stmt.close();
            }catch(SQLException se2){
                se2.printStackTrace();
            }
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
            }//end finally try
        }
        System.out.println("Goodbye!");

    }

    public static void insertEmployee(int id, int age, String first, String last, Connection conn, Statement stmt) {
        try{
            System.out.println("Inserting one row....");
            String SQL = "INSERT INTO Employees " +
                    "VALUES (" + id + ", " + age + ", '" + first + "', '" + last + "')";
            stmt.executeUpdate(SQL);

            // Commit data here.
            System.out.println("Commiting data here....");
            conn.commit();
        }catch(SQLException se){
            //Handle errors for JDBC
            se.printStackTrace();
            // If there is an error then rollback the changes.
            System.out.println("Rolling back data here....");
            try{
                if(conn!=null)
                    conn.rollback();
            }catch(SQLException se2){
                se2.printStackTrace();
            }//end try
        }
    }

    public static void add_salaries(
            ArrayList<Integer> ids,
            ArrayList<Integer> employee_ids,
            ArrayList<Integer> amount,
            Connection conn,
            Statement stmt) {

        try{
            // Check if employee_id already exists
            String sql = "SELECT employee_id FROM Salary";
            ResultSet rs = stmt.executeQuery(sql);
            ArrayList<Integer> lst = new ArrayList<>();

            rs.beforeFirst();
            while(rs.next()){
                int emp_id = rs.getInt("employee_id");
                lst.add(emp_id);
            }

            // Insert data if everything is OK
            for (int i = 0; i < ids.size(); i++) {
                if (lst.contains(employee_ids.get(i))) {
                    throw new SQLException("Conflicting employee ids!!! [" + employee_ids.get(i) + "]");
                }
                else {
                    System.out.println("Inserting one row....");
                    String SQL = "INSERT INTO Salary " +
                            "VALUES (" + ids.get(i) + "," +
                            employee_ids.get(i) + "," +
                            amount.get(i) + ")";
                    stmt.executeUpdate(SQL);
                }
            }
            rs.close();

            // Commit data here.
            System.out.println("Commiting data here....");
            conn.commit();
        }catch(SQLException se){
            //Handle errors for JDBC
            se.printStackTrace();
            // If there is an error then rollback the changes.
            System.out.println("Rolling back data here....");
            try{
                if(conn!=null)
                    conn.rollback();
            }catch(SQLException se2){
                se2.printStackTrace();
            }//end try
        }

    }

    public static void printRs(ResultSet rs) throws SQLException{
        //Ensure we start with first row
        rs.beforeFirst();
        while(rs.next()){
            //Retrieve by column name
            int id  = rs.getInt("id");
            int age = rs.getInt("age");
            String first = rs.getString("first");
            String last = rs.getString("last");

            //Display values
            System.out.print("ID: " + id);
            System.out.print(", Age: " + age);
            System.out.print(", First: " + first);
            System.out.println(", Last: " + last);
        }
        System.out.println();
    }

    public static void printSal(ResultSet rs) throws SQLException{
        rs.beforeFirst();
        while(rs.next()){
            int id  = rs.getInt("id");
            int emp_id = rs.getInt("employee_id");
            int am = rs.getInt("amount");

            //Display values
            System.out.print("ID: " + id);
            System.out.print(", Employee ID: " + emp_id);
            System.out.println(", Amount: " + am);
        }
        System.out.println();
    }
}
